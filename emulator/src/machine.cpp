
#include <algorithm>
#include <bitset>
#include <chrono>

#include <functional>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <limits>
#include <stdexcept>
#include <string>

#include <thread>


#include <endian.h>

#include "machine.h"

machine::machine(std::shared_ptr<gpu> gpu) 
    : m_gpu(gpu)
{
    std::fill(this->m_memory.begin(), this->m_memory.end(), 0);
    std::fill(this->m_reg_v.begin(), this->m_reg_v.end(), 0);
    this->m_pc = 512;
    
    this->m_rng.seed(std::random_device()());
    
    this->loadFonts();
}

machine::machine(const machine& orig) :
    m_memory(orig.m_memory),
    m_reg_v(orig.m_reg_v),
    m_delayTimer(orig.m_delayTimer),
    m_soundTimer(orig.m_soundTimer),
    m_pc(orig.m_pc)
{
}

machine::~machine() {
}

void machine::loadFonts(){
    
    std::vector<uint8_t> fonts = {
        0xF0, 0x90, 0x90, 0x90, 0xF0,   // 0     
        0x20, 0x60, 0x20, 0x20, 0x70,   // 1     
        0x20, 0x10, 0xF0, 0x80, 0xF0,   // 2    
        0xF0, 0x10, 0xF0, 0x10, 0xF0,   // 3    
        0x90, 0x90, 0xF0, 0x10, 0x10,   // 4   
        0xF0, 0x80, 0xF0, 0x10, 0xF0,   // 5  
        0xF0, 0x80, 0xF0, 0x90, 0xF0,   // 6
        0xF0, 0x10, 0x20, 0x40, 0x40,   // 7
        0xF0, 0x90, 0xF0, 0x90, 0xF0,   // 8
        0xF0, 0x90, 0xF0, 0x10, 0xF0,   // 9
        0xF0, 0x90, 0xF0, 0x90, 0x90,   // A
        0xE0, 0x90, 0xE0, 0x90, 0xE0,   // B
        0xF0, 0x80, 0x80, 0x80, 0xF0,   // C
        0xE0, 0x90, 0x90, 0x90, 0xE0,   // D
        0xF0, 0x80, 0xF0, 0x80, 0xF0,   // E
        0xF0, 0x80, 0xF0, 0x80, 0x80    // F
    };
    
    std::copy(fonts.begin(), fonts.end(), &this->m_memory[0]);
}


void machine::loadFile(std::string filename){
    std::ifstream testFile(filename, std::ios::binary);
    
    
    std::istreambuf_iterator<char> it(testFile);
    std::istreambuf_iterator<char> it_end;
    std::copy(it, it_end, &this->m_memory[512]);
}

void machine::dumpMemory(){
    std::cout << this->m_memory.size() << std::endl;
    for(size_t i=0; i<this->m_memory.size(); i=i+2){
        std::cout << std::dec << i << "\t" << std::hex;
        std::cout << static_cast<int>(this->m_memory.at(i))
                  << static_cast<int>(this->m_memory.at(i+1))
                  << std::endl;
    }
    std::cout << std::dec;
}



rawInstruction_t machine::instructionFetch(c8_address_register_t addr){
    rawInstruction_t result;
    result.m_full = be16toh(*(reinterpret_cast<uint16_t*>(&this->m_memory[addr])));
    return result;
}

std::shared_ptr<instruction> machine::instructionDecode(rawInstruction_t instr){
    using namespace std::placeholders;
    
    auto result = std::make_shared<instruction>(instr);

    uint16_t addr = result->m_addr;
    uint16_t immNN = result->m_immNN;
    uint16_t immN = result->m_immN;
    
    c8_x_value_t regXidx = result->m_regX_index;
    c8_x_value_t regYidx = result->m_regY_index;

    switch(result->getOpcode()){
        case C8_SYS:
            result->m_execution = (std::bind(&machine::executeSYS, this)); break;
        case C8_CLR:
            result->m_execution = (std::bind(&machine::executeCLR, this)); break;
        case C8_RTS:
            result->m_execution = (std::bind(&machine::executeRTS, this)); break;
        case C8_JUMP:          
            result->m_execution = (std::bind(&machine::executeJUMP, this, addr)); break;
        case C8_CALL:          
            result->m_execution = (std::bind(&machine::executeCALL, this, addr)); break;
        case C8_SKE:       
            result->m_execution = (std::bind(&machine::executeSKE, this, regXidx, immNN)); break;
        case C8_SKNE:
            result->m_execution = (std::bind(&machine::executeSKNE, this, regXidx, immNN)); break;
        case C8_SKRE:
            result->m_execution = (std::bind(&machine::executeSKRE, this, regXidx, regYidx)); break;
        case C8_LOAD:
            result->m_execution = (std::bind(&machine::executeLOAD, this, regXidx, immNN)); break;
        case C8_ADD:
            result->m_execution = (std::bind(&machine::executeADD, this, regXidx, immNN)); break;
        case C8_MOVE:
            result->m_execution = (std::bind(&machine::executeMOVE, this, regXidx, regYidx)); break;
        case C8_OR:
            result->m_execution = (std::bind(&machine::executeOR, this, regXidx, regYidx)); break;
        case C8_AND:
            result->m_execution = (std::bind(&machine::executeAND, this, regXidx, regYidx)); break;
        case C8_XOR:
            result->m_execution = (std::bind(&machine::executeXOR, this, regXidx, regYidx)); break;
        case C8_ADDR:
            result->m_execution = (std::bind(&machine::executeADDR, this, regXidx, regYidx)); break;
        case C8_SUB:
            result->m_execution = (std::bind(&machine::executeSUB, this, regXidx, regYidx)); break;
        case C8_SUBN:
            result->m_execution = (std::bind(&machine::executeSUBN, this, regXidx, regYidx)); break;
        case C8_SHR:
            result->m_execution = (std::bind(&machine::executeSHR, this, regXidx)); break;
        case C8_SHL:
            result->m_execution = (std::bind(&machine::executeSHL, this, regXidx)); break;
        case C8_SKRNE:
            result->m_execution = (std::bind(&machine::executeSKRNE, this, regXidx, regYidx)); break;
        case C8_LOADI:
            result->m_execution = (std::bind(&machine::executeLOADI, this, addr)); break;
        case C8_JUMPI:
            result->m_execution = (std::bind(&machine::executeJUMPI, this)); break;
        case C8_RAND:        
            result->m_execution = (std::bind(&machine::executeRAND, this, regXidx, immNN)); break;
        case C8_DRAW:   
            result->m_execution = (std::bind(&machine::executeDRAW, this, regXidx, regYidx, immN)); break;
        case C8_SKPR:
            result->m_execution = (std::bind(&machine::executeSKPR, this, regXidx)); break;
        case C8_SKUP:
            result->m_execution = (std::bind(&machine::executeSKUP, this, regXidx)); break;
        case C8_MOVED:
            result->m_execution = (std::bind(&machine::executeMOVED, this, regXidx)); break;
        case C8_KEYD:
            result->m_execution = (std::bind(&machine::executeKEYD, this, regXidx)); break;
        case C8_LOADD:
            result->m_execution = (std::bind(&machine::executeLOADD, this, regXidx)); break;
        case C8_LOADS:
            result->m_execution = (std::bind(&machine::executeLOADS, this, regXidx)); break;
        case C8_ADDI:
            result->m_execution = (std::bind(&machine::executeADDI, this, regXidx)); break;
        case C8_LDSPR:
            result->m_execution = (std::bind(&machine::executeLDSPR, this, regXidx)); break;
        case C8_BCD:
            result->m_execution = (std::bind(&machine::executeBCD, this, regXidx)); break;
        case C8_STOR:
            result->m_execution = (std::bind(&machine::executeSTOR, this, regXidx)); break;
        case C8_READ:
            result->m_execution = (std::bind(&machine::executeREAD, this, regXidx)); break;
        case C8_ILLEGAL:
            break;
        default:
            break;
    }
    return result;
}


void machine::execute(std::shared_ptr<instruction> instr){
    instr->m_execution();
}


void machine::step(){
    rawInstruction_t inst = this->instructionFetch(this->m_pc);

#if 1
    std::clog << this->m_pc << ":\t" 
                          << "0x"
                          << std::hex << std::setfill('0') << std::setw(2)
                          << static_cast<int>(inst.m_lower8) 
                          << std::hex << std::setfill('0') << std::setw(2)
                          << static_cast<int>(inst.m_upper8) 
                          << std::dec << std::endl;
#endif

    std::shared_ptr<instruction> instr = this->instructionDecode(inst);

#if 1
    std::clog << instr->toString() << std::endl;
#endif

    this->m_gpu->writeStatus(instr->toString().c_str());
    this->m_gpu->writeStatus("\n");
    this->m_gpu->refresh();
    
    if(this->m_delayTimer > 0){
        this->m_delayTimer--;
    }
    if(this->m_soundTimer > 0){
        this->m_soundTimer--;
    }

    this->m_currentlyPressedKeys = this->m_kybd.getCurrentlyPressedKeys();
            
    this->execute(instr);
}

void machine::run(){
    while(true){
        this->step();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

void machine::moveToNextInstruction(){
    this->m_pc += sizeof(rawInstruction_t);
}

c8_register_t& machine::getRegister(uint8_t regIndex){
    return this->m_reg_v.at(regIndex);
}


void machine::executeSYS(){
    /*
     * Do nothing, deliberately
     */       
    this->moveToNextInstruction();
}

void machine::executeCLR(){
    this->m_gpu->clearMain();
    this->moveToNextInstruction();
}

void machine::executeRTS(){
    if(this->m_stack.empty()){
        throw std::runtime_error("Stack underflow");
    }
    
    this->m_pc = this->m_stack.back();
    this->m_stack.pop_back();
    this->moveToNextInstruction();
}

void machine::executeJUMP(c8_nnn_value_t addr){
    this->m_pc = addr;
}

void machine::executeCALL(c8_nnn_value_t addr){
    this->moveToNextInstruction();
    this->m_stack.push_back(this->m_pc);
    this->m_pc = addr;
}

void machine::executeSKE(c8_x_value_t regXidx, c8_nn_value_t immediate){
    c8_register_t &regX = this->getRegister(regXidx);
    
    if(regX == immediate){
        this->moveToNextInstruction();
        this->moveToNextInstruction();
    }
    else {
        this->moveToNextInstruction();
    }
}

void machine::executeSKNE(c8_x_value_t regXidx, c8_nn_value_t immediate){
    c8_register_t &regX = this->getRegister(regXidx);

    if(regX != immediate){
        this->moveToNextInstruction();
        this->moveToNextInstruction();
    }
    else {
        this->moveToNextInstruction();
    }
}

void machine::executeSKRE(c8_x_value_t regXidx, c8_x_value_t regYidx){
    c8_register_t &regX = this->getRegister(regXidx);
    c8_register_t &regY = this->getRegister(regYidx);
    
    if(regX == regY){
        this->moveToNextInstruction();
        this->moveToNextInstruction();
    }
    else {
        this->moveToNextInstruction();
    }
}

void machine::executeLOAD(c8_x_value_t regXidx, c8_nn_value_t immediate){
    c8_register_t &regX = this->getRegister(regXidx);
    regX = immediate;       
    this->moveToNextInstruction();
}

void machine::executeADD(c8_x_value_t regXidx, c8_nn_value_t immediate){
    c8_register_t &regX = this->getRegister(regXidx);
    regX += immediate;       
    this->moveToNextInstruction();
}

/* Opcode-Nibble 0x9 start */
void machine::executeMOVE(c8_x_value_t regXidx, c8_x_value_t regYidx){
    c8_register_t &regX = this->getRegister(regXidx);
    c8_register_t &regY = this->getRegister(regYidx);
    regX = regY;       
    this->moveToNextInstruction();
}

void machine::executeOR(c8_x_value_t regXidx, c8_x_value_t regYidx){
    c8_register_t &regX = this->getRegister(regXidx);
    c8_register_t &regY = this->getRegister(regYidx);
    regX |= regY;       
    this->moveToNextInstruction();
}

void machine::executeAND(c8_x_value_t regXidx, c8_x_value_t regYidx){
    c8_register_t &regX = this->getRegister(regXidx);
    c8_register_t &regY = this->getRegister(regYidx);
    regX &= regY;       
    this->moveToNextInstruction();
}

void machine::executeXOR(c8_x_value_t regXidx, c8_x_value_t regYidx){
    c8_register_t &regX = this->getRegister(regXidx);
    c8_register_t &regY = this->getRegister(regYidx);
    regX ^= regY;       
    this->moveToNextInstruction();
}

void machine::executeADDR(c8_x_value_t regXidx, c8_x_value_t regYidx){
    c8_register_t &regX = this->getRegister(regXidx);
    c8_register_t &regY = this->getRegister(regYidx);
    
    if(__builtin_add_overflow(regX, regY, &regX)){
        this->m_reg_v.at(0xf) = 1;
    }
    else
    {
        this->m_reg_v.at(0xf) = 0;
    }       
    this->moveToNextInstruction();
    
}

void machine::executeSUB(c8_x_value_t regXidx, c8_x_value_t regYidx){
    c8_register_t &regX = this->getRegister(regXidx);
    c8_register_t &regY = this->getRegister(regYidx);

    if(regX > regY){
        this->m_reg_v.at(0xf) = 1;
    }
    else
    {
        this->m_reg_v.at(0xf) = 0;
    }
    
    regX -= regY;       
    this->moveToNextInstruction();
}

void machine::executeSHR(c8_x_value_t regXidx){
    c8_register_t &regX = this->getRegister(regXidx);
    
    if(regX & 1){
        this->m_reg_v.at(0xf) = 1;
    }
    else
    {
        this->m_reg_v.at(0xf) = 0;        
    }
    
    regX = regX >> 1;       
    this->moveToNextInstruction();
}

void machine::executeSUBN(c8_x_value_t regXidx, c8_x_value_t regYidx){
    c8_register_t &regX = this->getRegister(regXidx);
    c8_register_t &regY = this->getRegister(regYidx);

    if(regY > regX){
        this->m_reg_v.at(0xf) = 1;
    }
    else
    {
        this->m_reg_v.at(0xf) = 0;
    }
    
    regX = regY - regX;       
    this->moveToNextInstruction();
}

void machine::executeSHL(c8_x_value_t regXidx){
    c8_register_t &regX = this->getRegister(regXidx);
    
    if(regX & 0x80){
        this->m_reg_v.at(0xf) = 1;
    }
    else
    {
        this->m_reg_v.at(0xf) = 0;        
    }
    
    regX = regX << 1;
    
    this->moveToNextInstruction();
}

/* Opcode-Nibble 0x9 end */

void machine::executeSKRNE(c8_x_value_t regXidx, c8_x_value_t regYidx){
    c8_register_t &regX = this->getRegister(regXidx);
    c8_register_t &regY = this->getRegister(regYidx);

    if(regX != regY){
        this->moveToNextInstruction();
        this->moveToNextInstruction();
    }
    else {
        this->moveToNextInstruction();
    }
}

void machine::executeLOADI(c8_nnn_value_t addr){
    this->m_reg_i = addr;
    this->moveToNextInstruction();
}

void machine::executeJUMPI(){
    this->m_pc = this->m_reg_i;
    this->m_pc += this->getRegister(0x0);
}

void machine::executeRAND(c8_x_value_t regXidx, c8_nn_value_t immediate){
    c8_register_t &regX = this->getRegister(regXidx);

    std::uniform_int_distribution<std::mt19937::result_type> dist(
        std::numeric_limits<c8_register_t>::min(),
        std::numeric_limits<c8_register_t>::max()
    );
    
    regX = dist(this->m_rng);
    regX &= immediate;
    this->moveToNextInstruction();
}

void machine::executeDRAW(c8_x_value_t regXidx, c8_x_value_t regYidx, c8_n_value_t imm){  
    c8_register_t regX = this->getRegister(regXidx);
    c8_register_t regY = this->getRegister(regYidx);
    this->getRegister(0xf) = 0;
        
    for(c8_n_value_t n=0;n<imm;n++){
        c8_address_register_t addr = this->m_reg_i+n;

        std::bitset<8> currentByte(this->m_memory.at(addr));
        for(int i=0;i<currentByte.size();i++){
            
            int pos = 7-i;
            
            if(currentByte.test(i)){
            
                if(this->m_gpu->isSet(regX+pos, regY)){
                    this->m_gpu->clearPixel(regX+pos, regY);
                    this->getRegister(0xf) = 1;
                }
                else
                {
                    this->m_gpu->setPixel(regX+pos, regY);
                }
                
            }
            else
            {
                if(this->m_gpu->isSet(regX+pos, regY)){

                }
                // this->m_gpu->clearPixel(regX+pos, regY);                
            }
            
        }
        regY++;
    }
    this->moveToNextInstruction();
    this->m_gpu->refresh();
}

void machine::executeSKPR(c8_x_value_t regXidx){
    c8_register_t &regX = this->getRegister(regXidx);
    
    if(this->m_currentlyPressedKeys.find(regX) == this->m_currentlyPressedKeys.end()){
        this->moveToNextInstruction();
        this->moveToNextInstruction();        
    }
    
    this->moveToNextInstruction();
}

void machine::executeSKUP(c8_x_value_t regXidx){
    c8_register_t &regX = this->getRegister(regXidx);
    if(this->m_currentlyPressedKeys.find(regX) != this->m_currentlyPressedKeys.end()){
        this->moveToNextInstruction();
        this->moveToNextInstruction();        
    }
    this->moveToNextInstruction();
    
}

/* Opcode-Nibble 0xF start */
void machine::executeMOVED(c8_x_value_t regXidx){
    c8_register_t &regX = this->getRegister(regXidx);
    regX = this->m_delayTimer;
    this->moveToNextInstruction();
}

void machine::executeKEYD(c8_x_value_t regXidx){
    c8_register_t &regX = this->getRegister(regXidx);
    
    regX = this->m_kybd.getKeyPress();
    this->moveToNextInstruction();
}

void machine::executeLOADD(c8_x_value_t regXidx){
    c8_register_t &regX = this->getRegister(regXidx);
    this->m_delayTimer = regX;
    this->moveToNextInstruction();
}

void machine::executeLOADS(c8_x_value_t regXidx){
    c8_register_t &regX = this->getRegister(regXidx);
    this->m_soundTimer = regX;
    this->moveToNextInstruction();
}

void machine::executeADDI(c8_x_value_t regXidx){
    c8_register_t &regX = this->getRegister(regXidx);
    this->m_reg_i += regX;
    
    if(this->m_reg_i > 0xFFF){
        this->m_reg_v.at(0xf) = 1;
    }
    else
    {
        this->m_reg_v.at(0xf) = 0;
    }           
    
    this->moveToNextInstruction();
}

void machine::executeLDSPR(c8_x_value_t regXidx){
    c8_register_t &regX = this->getRegister(regXidx);
    this->m_reg_i = regX*5;
    this->moveToNextInstruction();
}

void machine::executeBCD(c8_x_value_t regXidx){
    c8_register_t regX = this->getRegister(regXidx);
    
    if(regX == 0){
        this->m_memory.at(this->m_reg_i) = 0;
        this->m_memory.at(this->m_reg_i+1) = 0;
        this->m_memory.at(this->m_reg_i+2) = 0;
    }
    else
    {
        for(int idx=2;idx>=0;idx--){
            uint8_t place = regX % 10;
            this->m_memory.at(this->m_reg_i+idx) = place;
            regX = regX / 10;            
        }
    }
    
    this->moveToNextInstruction();
}

void machine::executeSTOR(c8_x_value_t regXidx){
    for(c8_x_value_t i=0;i<=regXidx;i++){
        this->m_memory.at(this->m_reg_i+i) = this->getRegister(i);
    }
    this->m_reg_i += regXidx + 1;
    this->moveToNextInstruction();
}

void machine::executeREAD(c8_x_value_t regXidx){
    for(c8_x_value_t i=0;i<=regXidx;i++){
        this->getRegister(i) = this->m_memory.at(this->m_reg_i+i);
    }
    this->m_reg_i += regXidx + 1;
    this->moveToNextInstruction();
}

std::string machine::instToString(rawInstruction_t& instr){
    (void)instr;
    return std::string();
}
