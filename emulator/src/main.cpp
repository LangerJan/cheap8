#include <cstdlib>
#include <memory>

#include <SFML/Graphics.hpp>

#include "gpu.h"
#include "machine.h"

#include <chrono>
#include <thread>

int main(int argc, char* argv[]){
    
    std::shared_ptr<sf::RenderWindow> window(new sf::RenderWindow(sf::VideoMode (256, 128), "FOOBAR"));
    
    std::shared_ptr<gpu> graphics = std::make_shared<gpu>(window);
    std::shared_ptr<machine> logic = std::make_shared<machine>(graphics);
    
    logic->loadFile(argv[1]);
    
    //logic->run();
    
    
    // run the program as long as the window is open
    while (window->isOpen())
    {
        logic->step();
        
        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (window->pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
                window->close();
        }

        // clear the window with black color
        // window->clear(sf::Color::Black);

        // draw everything here...
        // window.draw(...);
        std::this_thread::sleep_for(std::chrono::milliseconds(3));

        // end the current frame
        window->display();
    }    
    
    
    return EXIT_SUCCESS;
}