
#ifndef GPU_H
#define GPU_H

#include <memory>
#include <vector>

#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Image.hpp>

class gpu {
public:
    gpu(std::shared_ptr<sf::RenderWindow> window);
    virtual ~gpu();
    
    void clearMain();
    
    bool isSet(uint8_t x, uint8_t y);
    
    void setPixel(uint8_t x, uint8_t y);
    void clearPixel(uint8_t x, uint8_t y);
    
    void refresh();
    
    void writeStatus(const char* str);
    
private:
    
    std::shared_ptr<sf::RenderWindow> m_window;
    sf::Image m_framebuffer;

};

#endif /* GPU_H */

