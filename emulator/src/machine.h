#ifndef MACHINE_H
#define MACHINE_H

#include <array>
#include <cstdint>
#include <deque>
#include <memory>
#include <random>
#include <string>

#include "gpu.h"
#include "instruction.h"
#include "c8types.h"
#include "keyboard.h"

class machine {
public:
    machine(std::shared_ptr<gpu> gpu);
    machine(const machine& orig);
    virtual ~machine();
    
    void loadFonts();
    
    void loadFile(std::string file);
    void dumpMemory();
    
    rawInstruction_t instructionFetch(c8_address_register_t addr);
    std::shared_ptr<instruction> instructionDecode(rawInstruction_t instr);
    
    void execute(std::shared_ptr<instruction> instr);
    
    void step();
    
    void run();
    
    c8_register_t& getRegister(uint8_t regIndex);
    
private:
    
    void moveToNextInstruction();
    
    std::array<uint8_t, 1024*4> m_memory;
    std::array<c8_register_t, 0x10> m_reg_v;

    c8_register_t m_delayTimer;
    c8_register_t m_soundTimer;

    c8_address_register_t m_reg_i;

    c8_address_register_t m_pc;
    
    std::deque<uint16_t> m_stack;
    
    std::shared_ptr<gpu> m_gpu;
    keyboard m_kybd;
    
    std::mt19937 m_rng;
    
    std::set<uint8_t> m_currentlyPressedKeys;
    
    void executeSYS();
    void executeCLR();
    void executeRTS();
    
    void executeJUMP(c8_nnn_value_t addr);
    void executeCALL(c8_nnn_value_t addr);
    void executeSKE(c8_x_value_t regXidx, c8_nn_value_t immediate);
    void executeSKNE(c8_x_value_t regXidx, c8_nn_value_t immediate);
    void executeSKRE(c8_x_value_t regXidx, c8_x_value_t regYidx);
    void executeLOAD(c8_x_value_t regXidx, c8_nn_value_t immediate);
    void executeADD(c8_x_value_t regXidx, c8_nn_value_t immediate);

    /* Opcode-Nibble 0x9 start */
    void executeMOVE(c8_x_value_t regXidx, c8_x_value_t regYidx);
    void executeOR(c8_x_value_t regXidx, c8_x_value_t regYidx);
    void executeAND(c8_x_value_t regXidx, c8_x_value_t regYidx);
    void executeXOR(c8_x_value_t regXidx, c8_x_value_t regYidx);
    void executeADDR(c8_x_value_t regXidx, c8_x_value_t regYidx);
    void executeSUB(c8_x_value_t regXidx, c8_x_value_t regYidx);
    void executeSHR(c8_x_value_t regXidx);
    void executeSUBN(c8_x_value_t regXidx, c8_x_value_t regYidx);
    void executeSHL(c8_x_value_t regXidx);
    /* Opcode-Nibble 0x9 end */
    
    void executeSKRNE(c8_x_value_t regXidx, c8_x_value_t regYidx);
    
    void executeLOADI(c8_nnn_value_t addr);
    
    void executeJUMPI();
    
    void executeRAND(c8_x_value_t regXidx, c8_nn_value_t immediate);
    
    void executeDRAW(c8_x_value_t regXidx, c8_x_value_t regYidx, c8_n_value_t imm);
        
    /* Opcode-Nibble 0xE start */
    void executeSKPR(c8_x_value_t regXidx);
    void executeSKUP(c8_x_value_t regXidx);    
    /* Opcode-Nibble 0xE end */
    
    /* Opcode-Nibble 0xF start */
    void executeMOVED(c8_x_value_t regXidx);
    void executeKEYD(c8_x_value_t regXidx);
    void executeLOADD(c8_x_value_t regXidx);
    void executeLOADS(c8_x_value_t regXidx);
    void executeADDI(c8_x_value_t regXidx);
    void executeLDSPR(c8_x_value_t regXidx);
    void executeBCD(c8_x_value_t regXidx);
    void executeSTOR(c8_x_value_t regXidx);
    void executeREAD(c8_x_value_t regXidx);
    /* Opcode-Nibble 0xF end */
    
    
    std::string instToString(rawInstruction_t& instr);
};

#endif /* MACHINE_H */

