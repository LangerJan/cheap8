#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <cstdint>
#include <set>

class keyboard {
public:
    keyboard();
    virtual ~keyboard();
    
    std::set<uint8_t> getCurrentlyPressedKeys();
    uint8_t getKeyPress();
private:

};

#endif /* KEYBOARD_H */

