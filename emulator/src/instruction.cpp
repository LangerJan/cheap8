#include "instruction.h"

#include <iostream>
#include <sstream>
#include <stdexcept>

instruction::instruction(rawInstruction_t instr) {

    uint16_t regX = instr.m_3rd_nibble;
    uint16_t regY = instr.m_2nd_nibble;

    //uint16_t regX = instr.m_2nd_nibble;
    //uint16_t regY = instr.m_3rd_nibble;

    
    uint16_t addr = instr.m_address;
    uint16_t immNN = instr.m_upper8;
    uint8_t immN = instr.m_1st_nibble;
    
    this->m_regX_index = regX;
    this->m_regY_index = regY;
    this->m_addr = instr.m_address;
    this->m_immNN = instr.m_upper8;
    this->m_immN = instr.m_1st_nibble;
    
    
    
    switch(instr.m_4th_nibble){
        case 0x0:
        {
            switch(instr.m_full){
                case 0x00E0:
                    this->m_mnemonic = std::string("CLR");
                    this->m_opcode = C8_CLR;
                    break;
                case 0x00EE:
                    this->m_mnemonic = std::string("RTS");
                    this->m_opcode = C8_RTS;
                    break;
                default:
                    this->m_mnemonic = std::string("SYS");
                    this->m_opcode = C8_SYS;
                    break;
            }
            break;
        }
        case 0x1:
            this->m_mnemonic = std::string("JUMP");
            this->m_opcode = C8_JUMP;
            this->m_arguments.push_back(addr);
            break;
        case 0x2:
            this->m_mnemonic = std::string("CALL");
            this->m_opcode = C8_CALL;
            this->m_arguments.push_back(addr);
            break;
        case 0x3:
            this->m_mnemonic = std::string("SKE");
            this->m_opcode = C8_SKE;
            this->m_arguments.push_back(regX);
            this->m_arguments.push_back(immNN);
            break;
        case 0x4:
            this->m_mnemonic = std::string("SKNE");
            this->m_opcode = C8_SKNE;
            this->m_arguments.push_back(regX);
            this->m_arguments.push_back(immNN);
            break;
        case 0x5:
            this->m_mnemonic = std::string("SKRE");
            this->m_opcode = C8_SKRE;
            this->m_arguments.push_back(regX);
            this->m_arguments.push_back(regY);
            break;
        case 0x6:
            this->m_mnemonic = std::string("LOAD");
            this->m_opcode = C8_LOAD;
            this->m_arguments.push_back(regX);
            this->m_arguments.push_back(immNN);
            break;
        case 0x7:
            this->m_mnemonic = std::string("ADD");
            this->m_opcode = C8_ADD;
            this->m_arguments.push_back(regX);
            this->m_arguments.push_back(immNN);
            break;
        case 0x8:
        {
            switch(instr.m_1st_nibble){
                case 0x0:
                    this->m_mnemonic = std::string("MOVE");
                    this->m_opcode = C8_MOVE;
                    this->m_arguments.push_back(regX);
                    this->m_arguments.push_back(regY);
                    break;
                case 0x1:
                    this->m_mnemonic = std::string("OR");
                    this->m_opcode = C8_OR;
                    this->m_arguments.push_back(regX);
                    this->m_arguments.push_back(regY);
                    break;
                case 0x2:
                    this->m_mnemonic = std::string("AND");
                    this->m_opcode = C8_AND;
                    this->m_arguments.push_back(regX);
                    this->m_arguments.push_back(regY);
                    break;
                case 0x3:
                    this->m_mnemonic = std::string("XOR");
                    this->m_opcode = C8_XOR;
                    this->m_arguments.push_back(regX);
                    this->m_arguments.push_back(regY);
                    break;
                case 0x4:
                    this->m_mnemonic = std::string("ADDR");
                    this->m_opcode = C8_ADDR;
                    this->m_arguments.push_back(regX);
                    this->m_arguments.push_back(regY);
                    break;
                case 0x5:
                    this->m_mnemonic = std::string("SUB");
                    this->m_opcode = C8_SUB;
                    this->m_arguments.push_back(regX);
                    this->m_arguments.push_back(regY);
                    break;
                case 0x6:
                    this->m_mnemonic = std::string("SHR");
                    this->m_opcode = C8_SHR;
                    this->m_arguments.push_back(regX);
                    break;
                case 0x7:
                    this->m_mnemonic = std::string("SUBN");
                    this->m_opcode = C8_SUBN;
                    this->m_arguments.push_back(regX);
                    this->m_arguments.push_back(regY);
                    break;
                case 0xE:
                    this->m_mnemonic = std::string("SHL");
                    this->m_opcode = C8_SHL;
                    this->m_arguments.push_back(regX);
                    break;
                default:
                    std::cerr << instr.m_full << std::endl;
                    std::cerr << std::hex << static_cast<int>(instr.m_4th_nibble)
                                 << "--" << 
                            static_cast<int>(instr.m_1st_nibble) << std::endl;
                    throw std::runtime_error("Invalid opcode");
            }
            break;
        }
        case 0x9:
            this->m_mnemonic = std::string("SKRNE");
            this->m_opcode = C8_SKRNE;
            this->m_arguments.push_back(regX);
            this->m_arguments.push_back(regY);
            break;
        case 0xa:
            this->m_mnemonic = std::string("LOADI");
            this->m_opcode = C8_LOADI;
            this->m_arguments.push_back(addr);
            break;
        case 0xb:
            this->m_mnemonic = std::string("JUMPI");
            this->m_opcode = C8_JUMPI;
            this->m_arguments.push_back(addr);
            break;
        case 0xc:
            this->m_mnemonic = std::string("RAND");
            this->m_opcode = C8_RAND;
            this->m_arguments.push_back(regX);
            this->m_arguments.push_back(immNN);
            break;
        case 0xd:
            this->m_mnemonic = std::string("DRAW");
            this->m_opcode = C8_DRAW;
            this->m_arguments.push_back(regX);
            this->m_arguments.push_back(regY);
            this->m_arguments.push_back(immN);
            break;
        case 0xe:
        {
            switch(instr.m_upper8){
                case 0x9E:
                    this->m_mnemonic = std::string("SKPR");
                    this->m_opcode = C8_SKPR;
                    this->m_arguments.push_back(regX);
                    break;
                case 0xA1:
                    this->m_mnemonic = std::string("SKUP");
                    this->m_opcode = C8_SKUP;
                    this->m_arguments.push_back(regX);
                    break;
                default:
                    std::cerr << instr.m_full << std::endl;
                    throw std::runtime_error("Invalid opcode");
            }
            break;
        }
        case 0xf:
        {
            this->m_arguments.push_back(regX);
            switch(instr.m_upper8){
                case 0x07:
                    this->m_mnemonic = std::string("MOVED");
                    this->m_opcode = C8_MOVED;
                    break;
                case 0x0A:
                    this->m_mnemonic = std::string("KEYD");
                    this->m_opcode = C8_KEYD;
                    break;
                case 0x15:
                    this->m_mnemonic = std::string("LOADD");
                    this->m_opcode = C8_LOADD;
                    break;
                case 0x18:
                    this->m_mnemonic = std::string("LOADS");
                    this->m_opcode = C8_LOADS;
                    break;
                case 0x1E:
                    this->m_mnemonic = std::string("ADDI");
                    this->m_opcode = C8_ADDI;
                    break;
                case 0x29:
                    this->m_mnemonic = std::string("LDSPR");
                    this->m_opcode = C8_LDSPR;
                    break;
                case 0x33:
                    this->m_mnemonic = std::string("BCD");
                    this->m_opcode = C8_BCD;
                    break;
                case 0x55:
                    this->m_mnemonic = std::string("STOR");
                    this->m_opcode = C8_STOR;
                    break;
                case 0x65:
                    this->m_mnemonic = std::string("READ");
                    this->m_opcode = C8_READ;
                    break;
                default:
                    std::cerr << instr.m_full << std::endl;
                    std::cerr << std::hex << instr.m_4th_nibble << instr.m_1st_nibble << std::endl;
                    throw std::runtime_error("Invalid opcode");
            }
            break;
        }
        default:
            std::cerr << instr.m_full << std::endl;
            std::cerr << std::hex << instr.m_4th_nibble << instr.m_1st_nibble << std::endl;
            throw std::runtime_error("Invalid opcode");
    }
}

instruction::instruction(const instruction& orig) 
    :   m_mnemonic(orig.m_mnemonic),
        m_arguments(orig.m_arguments)
{}

instruction::~instruction() {
}

c8_opcode_t instruction::getOpcode(){
    return this->m_opcode;
}

std::string instruction::toString(){
    std::stringstream ss;
    ss << this->m_mnemonic << " ";
    for(auto elem : this->m_arguments){
        ss << elem << " ";
    }
    return ss.str();
}
