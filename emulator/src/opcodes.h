#ifndef OPCODES_H
#define OPCODES_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum c8_opcode {
    C8_SYS,
    C8_CLR,
    C8_RTS,
    C8_JUMP,
    C8_CALL,
    C8_SKE,
    C8_SKNE,
    C8_SKRE,
    C8_LOAD,
    C8_ADD,
    C8_MOVE,
    C8_OR,
    C8_AND,
    C8_XOR,
    C8_ADDR,
    C8_SUB,
    C8_SHR,
    C8_SUBN,
    C8_SHL,
    C8_SKRNE,
    C8_LOADI,
    C8_JUMPI,
    C8_RAND,
    C8_DRAW,
    C8_SKPR,
    C8_SKUP,
    C8_MOVED,
    C8_KEYD,
    C8_LOADD,
    C8_LOADS,
    C8_ADDI,
    C8_LDSPR,
    C8_BCD,
    C8_STOR,
    C8_READ,
    C8_ILLEGAL
} c8_opcode_t;



#ifdef __cplusplus
}
#endif

#endif /* OPCODES_H */

