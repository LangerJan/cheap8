#include <iostream>

#include "gpu.h"

gpu::gpu(std::shared_ptr<sf::RenderWindow> window)
    : m_window(window)
{
    this->m_framebuffer.create(64, 32, sf::Color::Black);
    
}

gpu::~gpu() {
    
}

void gpu::clearMain(){
    for(int x=0;x<64;x++){
        for(int y=0;y<32;y++){
            this->m_framebuffer.setPixel(x, y, sf::Color::Black);    
        }
    }
}

bool gpu::isSet(uint8_t x, uint8_t y){
    x = x % 64;
    y = y % 32;
    
    sf::Color col = this->m_framebuffer.getPixel(x, y);
    
    return col.r == sf::Color::White.r &&
            col.g == sf::Color::White.g &&
            col.b == sf::Color::White.b;
}


void gpu::setPixel(uint8_t x, uint8_t y){
    x = x % 64;
    y = y % 32;

    this->m_framebuffer.setPixel(x, y, sf::Color::White);
}

void gpu::clearPixel(uint8_t x, uint8_t y){
    x = x % 64;
    y = y % 32;

    this->m_framebuffer.setPixel(x, y, sf::Color::Black);
}

void gpu::writeStatus(const char* str){
    
}

void gpu::refresh(){
    sf::Texture texture;
    
    texture.create(512, 256);
    
    
    texture.loadFromImage(this->m_framebuffer);
    sf::Sprite background(texture);
    background.scale(6.0, 6.0);
    this->m_window->clear(sf::Color::Black);
    this->m_window->draw(background);
    
}
