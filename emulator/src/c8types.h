/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   c8types.h
 * Author: jga
 *
 * Created on 8. November 2018, 10:56
 */

#ifndef C8TYPES_H
#define C8TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

#include <cstdint>
    
typedef uint8_t c8_register_t;
typedef uint16_t c8_address_register_t;

typedef uint16_t c8_nnn_value_t;
typedef uint8_t c8_nn_value_t;
typedef uint8_t c8_n_value_t;
typedef uint8_t c8_x_value_t;
typedef uint8_t c8_y_value_t;


#ifdef __cplusplus
}
#endif

#endif /* C8TYPES_H */

