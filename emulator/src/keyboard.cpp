#include "keyboard.h"

#include <SFML/Window/Keyboard.hpp>

keyboard::keyboard() {
}


keyboard::~keyboard() {
}

std::set<uint8_t> keyboard::getCurrentlyPressedKeys(){
    std::set<uint8_t> result;
    
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num1)) result.insert(0x1);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) result.insert(0x2);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num3)) result.insert(0x3);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) result.insert(0x4);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::W)) result.insert(0x5);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::E)) result.insert(0x6);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)) result.insert(0x7);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)) result.insert(0x8);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)) result.insert(0x9);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Y)) result.insert(0xa);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::X)) result.insert(0x0);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::C)) result.insert(0xb);
    
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num4)) result.insert(0xc);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::R)) result.insert(0xd);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::F)) result.insert(0xe);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::V)) result.insert(0xf);
    
    return result;
}

uint8_t keyboard::getKeyPress(){
    std::set<uint8_t> pressedKeys;
    
    do{
        pressedKeys = this->getCurrentlyPressedKeys();
    }while(pressedKeys.empty());
    
    return *pressedKeys.begin();
}
