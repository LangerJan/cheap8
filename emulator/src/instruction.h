#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include <cstdint>
#include <functional>
#include <string>
#include <vector>

#include "opcodes.h"
#include "c8types.h"

typedef union rawInstruction{
    uint16_t m_full;
    struct {
        uint8_t m_upper8;
        uint8_t m_lower8;
    };
    struct {
        uint8_t m_1st_nibble : 4;
        uint8_t m_2nd_nibble : 4;
        uint8_t m_3rd_nibble : 4;
        uint8_t m_4th_nibble : 4;
    };
    struct {
        uint16_t m_address : 12;
        uint8_t m_address_opcode : 4;        
    };
} rawInstruction_t;

class instruction {
public:
    instruction(rawInstruction_t instr);
    instruction(const instruction& orig);
    
    c8_opcode_t getOpcode();    
    virtual ~instruction();
    
    std::string toString();
    
    rawInstruction_t m_instr;
    std::function<void()> m_execution;

    c8_x_value_t m_regX_index;
    c8_y_value_t m_regY_index;
    uint16_t m_addr;
    uint16_t m_immNN;
    uint16_t m_immN;
    
private:

    c8_opcode_t m_opcode;
    std::string m_mnemonic;
    std::vector<uint16_t> m_arguments;
    
};

#endif /* INSTRUCTION_H */

